function makeTable(tWidth, tHeight) {
    // Make a table from width and height.
    table = document.querySelector("#game");

    for (let row=0; row<tHeight; row++) {
        text += '\n<tr id="row">';
        
        for (let column=0; column<tWidth; column++) {
            text += `\n\t<td id="Cell${row}x${column}"></td>`;
        }
        text += "\n</tr>";
    }

    table.innerHTML = text;
}

function clearTable(className) {
    // Clear a specified class of the table.
    while (document.querySelector(`.${className}`)) {
        document.querySelector(`.${className}`).classList.remove(className)
    }
}

function setClass(cellRow, cellCol, className) {
    // Reset all classes and set new one for a cell.
    let cell = document.querySelector(`#Cell${cellRow}x${cellCol}`);
    cell.className = "" // Reset classes
    addClass(cellRow, cellCol, className);
}

function addClass(cellRow, cellCol, className) {
    // Add Specified class to cell with specified position.
    let cell = document.querySelector(`#Cell${cellRow}x${cellCol}`);
    cell.classList.add(className);
}


function removeClass(cellRow, cellCol, className) {
    // Remove one class from a cell
    let cell = document.querySelector(`#Cell${cellRow}x${cellCol}`);
    cell.classList.remove(className);
}

function drawRect(cornerRow, cornerCol, width, height, className) {
    // Draw a rectangle on the table.
    for (let x = 0; x < width; x++) {
        for (let y = 0; y < height; y++) {
            let cellPosRow = cornerRow + x
            let cellPosCol = cornerCol + y
            setClass(cellPosRow, cellPosCol, className)
        }
    }
}

