let width = 64;
let height = 64;
let text = "";

let table;

window.onload = function () {
  // Anonymous function, generates a table from the utils function
  table = document.querySelector("game");
  makeTable(width, height);
  let data = mazeGen();
  console.log(data);
  setTable(data);
  // mazeGen(0, 0)
};

function setTable(tableArray) {
  // Set table classes based on 2d array
  for (let row = 0; row < height; row++) {
    for (let col = 0; col < width; col++) {
      let cell = document.querySelector(`#Cell${row}x${col}`);
      let tableValue = tableArray[row][col];
      if (tableValue === 0) {
        cell.classList.add("wall");
      } else {
        cell.classList.add("passage");
      }
    }
  }
}

function getSurroundingCells(cell) {
  let cellRow = cell[0];
  let cellCol = cell[1];
  let surrounding = [];

  if (cellRow > 0) {
    surrounding.push([cellRow - 1, cellCol]);
  }
  if (cellRow < height - 1) {
    surrounding.push([cellRow + 1, cellCol]);
  }
  if (cellCol > 0) {
    surrounding.push([cellRow, cellCol - 1]);
  }
  if (cellCol < width - 1) {
    surrounding.push([cellRow, cellCol + 1]);
  }
  return surrounding;
}

function mazeGen() {
  // Make a maze using the randomised Prim's algorithm
  var mazeTable = []; // Make a 2D array the size of our table, 0 means wall, 1 means passage
  for (let i = 0; i < height; i++) {
    mazeTable[i] = [];
    for (let j = 0; j < width; j++) {
      mazeTable[i][j] = 0;
    }
  }

  //   let startRow = Math.floor(Math.random() * height);
  //   let startCol = Math.floor(Math.random() * width);

  let startRow = 0;
  let startCol = 0;

  let visited = [[startRow, startCol]];
  let cells = getSurroundingCells([startRow, startCol]);
  while (cells.length > 0) {
    // Pick a random cell from the list.
    let randomIndex = Math.floor(Math.random() * cells.length);
    let pickedCell = cells[randomIndex];
    let row = pickedCell[0];
    let column = pickedCell[1];
    let surrounding = getSurroundingCells(pickedCell);

    let visitedAround = 0;
    for (let i = 0; i < surrounding.length; i++) {
      let cellPos = surrounding[i];

      for (let j = 0; j < visited.length; j++) {
        if (visited[j][0] === cellPos[0] && visited[j][1] === cellPos[1]) {
          visitedAround++;
        }
      }
      //   if (visited.includes(cellPos) || visited == cellPos) {
      //     console.log("Found");
      //     visitedAround++;
      //   }
    }

    cells.splice(randomIndex, 1);

    if (visitedAround === 1) {
      mazeTable[row][column] = 1;
      visited.push([row, column]);

      // Add the neighboring walls of the cell to the wall list.
      let wallSurroundings = getSurroundingCells([row, column]);
      for (let i = 0; i < wallSurroundings.length; i++) {
        let currentCell = wallSurroundings[i];
        let cellValue = mazeTable[currentCell[0]][currentCell[1]];
        if (cellValue === 0) {
          cells.push(currentCell);
        }
      }
      //   cells = cells.concat(getSurroundingCells([row, column]));
    }
  }
  return mazeTable;
}
// function mazeGen() {
//   // Make a maze using the randomised Prim's algorithm
//   var mazeTable = []; // Make a 2D array the size of our table, 0 means wall, 1 means passage
//   for (let i = 0; i < height; i++) {
//     mazeTable[i] = [];
//     for (let j = 0; j < width; j++) {
//       mazeTable[i][j] = 0;
//     }
//   }

//   // Pick the start position, pick a random index (0-7)
//   let startRow = Math.floor(Math.random() * height);
//   let startCol = Math.floor(Math.random() * width);

//   let cells = [[startRow, startCol]]; // Declare an array of cell positions, with the initial cell position as its first value

//   while (cells.length > 0) {
//     // Pick a random cell, then set it as a passage

//     // Pick a random wall from the list.
//     let randomIndex = Math.floor(Math.random() * cells.length);
//     let pickedCell = cells[randomIndex];
//     let row = pickedCell[0];
//     let column = pickedCell[1];

//     mazeTable[row][column] = 1;
//     // console.log(mazeTable[row][column]);

//     let neighbours = []; // This is an array that stores only walls, so unvisited cells, which we can check.
//     if (row > 0 && mazeTable[row - 1][column] === 0) {
//       neighbours.push([row - 1, column]); // If cell has a neighbour one cell up and the neighbour is a wall, add it to the list.
//     }

//     if (row < height - 1 && mazeTable[row + 1][column] === 0) {
//       neighbours.push([row + 1, column]); // If cell has a neighbour one cell down and the neighbour is a wall, add it to the list.
//     }

//     if (column > 0 && mazeTable[row][column - 1] === 0) {
//       neighbours.push([row, column - 1]); // If cell has a neighbour on the left and the neighbour is a wall, add it to the list.
//     }

//     if (column < width - 1 && mazeTable[row][column + 1] === 0) {
//       neighbours.push([row, column + 1]); // If cell has a neighbour on the right and the neighbour is a wall, add it to the list.
//     }

//     let neighbourIndex = Math.floor(Math.random() * neighbours.length);
//     let pickedNeigbour = neighbours[neighbourIndex];
//     let pickedRow = pickedNeigbour[0];
//     let pickedColumn = pickedNeigbour[1];
//     if (cells[pickedRow][pickedColumn] === 1) {
//     console.log("Failed");
//     return;
//     }
//     console.log(pickedNeigbour);
//     cells.splice(neighbourIndex, 1); // Remove picked neighbour from cells
//     mazeTable[pickedRow][pickedColumn] = 1; // Make the picked neighbour a passage

//     // Check the neighbour's neighbours
//     let visitedNeighbours = 0;
//     let pickedNeighbours = [];
//     if (pickedRow > 0) {
//     if (mazeTable[pickedRow - 1][pickedColumn] === 1) {
//         visitedNeighbours++;
//     }
//     pickedNeighbours.push([pickedRow - 1, pickedColumn]);
//     }

//     if (pickedRow < height - 1) {
//     if (mazeTable[pickedRow + 1][pickedColumn] === 1) {
//         visitedNeighbours++;
//     } else {
//         pickedNeighbours.push([pickedRow + 1, pickedColumn]);
//     }
//     }

//     if (pickedColumn > 0 && mazeTable[pickedRow][pickedColumn - 1] === 1) {
//     if (mazeTable[pickedRow][pickedColumn - 1] === 1) {
//         visitedNeighbours++;
//     } else {
//         pickedNeighbours.push([pickedRow, pickedColumn - 1]);
//     }
//     }

//     if (
//     pickedColumn < width - 1 &&
//     mazeTable[pickedRow][pickedColumn + 1] === 1
//     ) {
//     if (mazeTable[pickedRow][pickedColumn + 1] === 1) {
//         visitedNeighbours++;
//     } else {
//         pickedNeighbours.push([pickedRow, pickedColumn + 1]);
//     }
//     }

//     cells.splice(neighbourIndex, 1); // Remove the cell from the cell array
//     if (visitedNeighbours === 1) {
//     console.log("Visited");
//     mazeTable[pickedRow][pickedColumn] = 1;
//     cells = cells.concat(pickedNeighbours);
//     }
// }
// }
// console.log("Finished");
// return mazeTable;
